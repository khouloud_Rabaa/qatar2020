package isi.tn.qatar2020project.entities;

import javax.persistence.*;
import java.io.Serializable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity

public class Spectateur implements Serializable {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)

    private int idspect;
    private String nomspcet;
    private String prenomspect;
    private String adressespect;
    private int cinspect;
    private int agespect;


    @ManyToOne(fetch = FetchType.LAZY)
    private Equipe equipe;
    public Spectateur() {
    }

    public int getIdspect() {
        return idspect;
    }

    public void setIdspect(int idspect) {
        this.idspect = idspect;
    }

    public String getNomspcet() {
        return nomspcet;
    }

    public void setNomspcet(String nomspcet) {
        this.nomspcet = nomspcet;
    }

    public String getPrenomspect() {
        return prenomspect;
    }

    public void setPrenomspect(String prenomspect) {
        this.prenomspect = prenomspect;
    }

    public String getAdressespect() {
        return adressespect;
    }

    public void setAdressespect(String adressespect) {
        this.adressespect = adressespect;
    }

    public int getCinspect() {
        return cinspect;
    }

    public void setCinspect(int cinspect) {
        this.cinspect = cinspect;
    }

    public int getAgespect() {
        return agespect;
    }

    public void setAgespect(int agespect) {
        this.agespect = agespect;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }
}
