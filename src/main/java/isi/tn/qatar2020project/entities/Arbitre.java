package isi.tn.qatar2020project.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Arbitre implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id ;
    private String nomarbitre;
    private String prenomarbitre;
    private String adressearbitre;
    private String nationnalitearbitre;
    private int cinarbitre;
    private Date datenaissancearbitre;

    public Arbitre() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomarbitre() {
        return nomarbitre;
    }

    public void setNomarbitre(String nomarbitre) {
        this.nomarbitre = nomarbitre;
    }

    public String getPrenomarbitre() {
        return prenomarbitre;
    }

    public void setPrenomarbitre(String prenomarbitre) {
        this.prenomarbitre = prenomarbitre;
    }

    public String getAdressearbitre() {
        return adressearbitre;
    }

    public void setAdressearbitre(String adressearbitre) {
        this.adressearbitre = adressearbitre;
    }

    public String getNationnalitearbitre() {
        return nationnalitearbitre;
    }

    public void setNationnalitearbitre(String nationnalitearbitre) {
        this.nationnalitearbitre = nationnalitearbitre;
    }

    public int getCinarbitre() {
        return cinarbitre;
    }

    public void setCinarbitre(int cinarbitre) {
        this.cinarbitre = cinarbitre;
    }

    public Date getDatenaissancearbitre() {
        return datenaissancearbitre;
    }

    public void setDatenaissancearbitre(Date datenaissancearbitre) {
        this.datenaissancearbitre = datenaissancearbitre;
    }
}
