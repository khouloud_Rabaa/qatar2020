package isi.tn.qatar2020project.entities;


import org.hibernate.exception.DataException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Ticket implements Serializable {
    @Id
    @GeneratedValue
    private Long idticket;
    private Date dateachatticket;
    private float prixticket;
    @ManyToOne(fetch = FetchType.LAZY)
    private Match match;
    @ManyToOne(fetch = FetchType.LAZY)
    private Spectateur spectateur;

    public Ticket() {
    }

    public Long getIdticket() {
        return idticket;
    }

    public void setIdticket(Long idticket) {
        this.idticket = idticket;
    }

    public Date getDateachatticket() {
        return dateachatticket;
    }

    public void setDateachatticket(Date dateachatticket) {
        this.dateachatticket = dateachatticket;
    }

    public float getPrixticket() {
        return prixticket;
    }

    public void setPrixticket(float prixticket) {
        this.prixticket = prixticket;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Spectateur getSpectateur() {
        return spectateur;
    }

    public void setSpectateur(Spectateur spectateur) {
        this.spectateur = spectateur;
    }
}
