package isi.tn.qatar2020project.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Match implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date datematch;
    private String heurematch;
    private String lieumatch;
    private int capacitematch;

    public Match() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatematch() {
        return datematch;
    }

    public void setDatematch(Date datematch) {
        this.datematch = datematch;
    }

    public String getHeurematch() {
        return heurematch;
    }

    public void setHeurematch(String heurematch) {
        this.heurematch = heurematch;
    }

    public String getLieumatch() {
        return lieumatch;
    }

    public void setLieumatch(String lieumatch) {
        this.lieumatch = lieumatch;
    }

    public int getCapacitematch() {
        return capacitematch;
    }

    public void setCapacitematch(int capacitematch) {
        this.capacitematch = capacitematch;
    }
}
