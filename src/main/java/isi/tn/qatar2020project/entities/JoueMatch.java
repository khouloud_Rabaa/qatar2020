package isi.tn.qatar2020project.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class JoueMatch implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idjouematch;
    @ManyToOne(fetch = FetchType.LAZY)
    private Arbitre arbitre;
    @OneToMany
    private Collection<Equipe> equipes;

    public JoueMatch() {
    }

    public int getIdjouematch() {
        return idjouematch;
    }

    public void setIdjouematch(int idjouematch) {
        this.idjouematch = idjouematch;
    }

    public Arbitre getArbitre() {
        return arbitre;
    }

    public void setArbitre(Arbitre arbitre) {
        this.arbitre = arbitre;
    }

    public Collection<Equipe> getEquipes() {
        return equipes;
    }

    public void setEquipes(Collection<Equipe> equipes) {
        this.equipes = equipes;
    }
}

