package isi.tn.qatar2020project.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Equipe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idequipe;
    private String nomequipe;
    private int tailleequipe;
    private String gardienequipe;
    private Date datefondationequipe;

    public Equipe() {
    }

    public Long getIdequipe() {
        return idequipe;
    }

    public String getNomequipe() {
        return nomequipe;
    }

    public int getTailleequipe() {
        return tailleequipe;
    }

    public String getGardienequipe() {
        return gardienequipe;
    }

    public Date getDatefondationequipe() {
        return datefondationequipe;
    }

    public void setIdequipe(Long idequipe) {
        this.idequipe = idequipe;
    }

    public void setNomequipe(String nomequipe) {
        this.nomequipe = nomequipe;
    }

    public void setTailleequipe(int tailleequipe) {
        this.tailleequipe = tailleequipe;
    }

    public void setGardienequipe(String gardienequipe) {
        this.gardienequipe = gardienequipe;
    }

    public void setDatefondationequipe(Date datefondationequipe) {
        this.datefondationequipe = datefondationequipe;
    }
}
