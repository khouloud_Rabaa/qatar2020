package isi.tn.qatar2020project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Qatar2020projectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Qatar2020projectApplication.class, args);
    }

}
