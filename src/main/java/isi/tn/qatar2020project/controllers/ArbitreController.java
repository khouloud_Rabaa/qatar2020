package isi.tn.qatar2020project.controllers;

import isi.tn.qatar2020project.entities.Arbitre;
import isi.tn.qatar2020project.repositories.ArbitreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class ArbitreController {

    @Autowired
    private ArbitreRepository arbitreRepository;






    @RequestMapping(value="/indexArbitre")
    public String index(Model model,
                        @RequestParam(name="page",defaultValue="0")int p,
                        @RequestParam (name="size",defaultValue="2")int s,
                        @RequestParam (name="motCle",defaultValue="")String mc){

        Page<Arbitre> pageArbitre=
                arbitreRepository.chercher("%"+mc+"%",new PageRequest(p,s));


        model.addAttribute("listArbitre", pageArbitre.getContent());
        int[] pages=new int[pageArbitre.getTotalPages()];
        model.addAttribute("pages",pages);
        model.addAttribute("size", s);
        model.addAttribute("pageCourante", p);
        model.addAttribute("motCle", mc);
        return "arbitre";
    }

    @RequestMapping(value="/deleteArbitre",method= RequestMethod.GET)
    public String delete(int id,String motCle,int page,int size){
        arbitreRepository.delete(id);
        return "redirect:/indexArbitre?page="+page+"&size="+size+"&motCle="+motCle;

    }

    @RequestMapping(value="/editArbitre",method=RequestMethod.GET)
    public String edit(Model model,int id){
        Optional<Arbitre> p=arbitreRepository.findById((long) id);
        model.addAttribute("arbitre", p);
        return "EditArbitre";

    }
    @RequestMapping(value="/formEditArbitre",method=RequestMethod.GET)
    public String formarbitre (Model model ){
        model.addAttribute("arbitre", new Arbitre());

        return "FormArbitre";
    }
    @RequestMapping(value="/saveArbitre",method=RequestMethod.POST)
    public String save(Model model , @Valid Arbitre arbitre, BindingResult bindingResult ){
        if(bindingResult.hasErrors())
            return "FormArbitre";
        arbitreRepository.save(arbitre);

        return "redirect:/indexArbitre";

    }


}
