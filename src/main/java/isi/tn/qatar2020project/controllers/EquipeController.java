package isi.tn.qatar2020project.controllers;

 import isi.tn.qatar2020project.entities.Equipe;
 import isi.tn.qatar2020project.repositories.EquipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class EquipeController {

    @Autowired
    private EquipeRepository equipeRepository;






    @RequestMapping(value="/indexEquipe")
    public String index(Model model,
                        @RequestParam(name="page",defaultValue="0")int p,
                        @RequestParam (name="size",defaultValue="2")int s,
                        @RequestParam (name="motCle",defaultValue="")String mc){

        Page<Equipe> pageEquipe=
                equipeRepository.chercher("%"+mc+"%",new PageRequest(p,s));


        model.addAttribute("listEquipe", pageEquipe.getContent());
        int[] pages=new int[pageEquipe.getTotalPages()];
        model.addAttribute("pages",pages);
        model.addAttribute("size", s);
        model.addAttribute("pageCourante", p);
        model.addAttribute("motCle", mc);
        return "equipe";
    }

    @RequestMapping(value="/deleteequipe",method= RequestMethod.GET)
    public String delete(int id,String motCle,int page,int size){
        equipeRepository.delete(id);
        return "redirect:/indexEquipe?page="+page+"&size="+size+"&motCle="+motCle;

    }

    @RequestMapping(value="/editequipe",method=RequestMethod.GET)
    public String edit(Model model,int id){
        Optional<Equipe> p=equipeRepository.findById((long) id);
        model.addAttribute("equipe", p);
        return "Editequipe";

    }
    @RequestMapping(value="/formEditequipe",method=RequestMethod.GET)
    public String Formequipe (Model model ){
        model.addAttribute("equipe", new Equipe());

        return "Formequipe";
    }
    @RequestMapping(value="/saveequipe",method=RequestMethod.POST)
    public String save(Model model , @Valid Equipe equipe, BindingResult bindingResult ){
        if(bindingResult.hasErrors())
            return "Formequipe";
        equipeRepository.save(equipe);

        return "redirect:/indexequipe";

    }
}
