package isi.tn.qatar2020project.controllers;

import isi.tn.qatar2020project.entities.Spectateur;
import isi.tn.qatar2020project.repositories.EquipeRepository;
import isi.tn.qatar2020project.repositories.SpectateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class SpectateurController {
    @Autowired
    private SpectateurRepository spectateurRepository;

    @Autowired
    private EquipeRepository equipeRepository;

    @RequestMapping(value="/indexSpectateur")
    public String index(Model model,
                        @RequestParam(name="page",defaultValue="0")int p,
                        @RequestParam (name="size",defaultValue="4")int s,
                        @RequestParam (name="motCle",defaultValue="")String mc){

        Page<Spectateur> pageSpectateur=
                spectateurRepository.chercher("%"+mc+"%",new PageRequest(p,s));
        model.addAttribute("listSpectateurs", pageSpectateur.getContent());
        int[] pages=new int[pageSpectateur.getTotalPages()];
        model.addAttribute("pages",pages);
        model.addAttribute("size", s);
        model.addAttribute("pageCourante", p);
        model.addAttribute("motCle", mc);
        return "spectateur";
    }

    @RequestMapping(value="/deletespectateur",method= RequestMethod.GET)
    public String delete(Model model,Long id,String motCle,int page,int size){
        spectateurRepository.delete(id);
        model.addAttribute("spectateurs",  spectateurRepository.findAll());
        model.addAttribute("equipe",  equipeRepository.findAll());
        return "redirect:/indexSpectateur?page="+page+"&size="+size+"&motCle="+motCle;

    }

    @RequestMapping(value="/editspectateur",method=RequestMethod.GET)
    public String edit(Model model,Long id){
        Spectateur p;
        p = spectateurRepository.findOne(id);
        model.addAttribute("spectateur",  spectateurRepository.findOne(id));
        model.addAttribute("spectateurs",  spectateurRepository.findAll());
        model.addAttribute("equipe",  equipeRepository.findAll());

        model.addAttribute("spectateur", p);
        return "editspectateur";



    }
    @RequestMapping(value="/formSpectateur",method=RequestMethod.GET)
    public String formPays(Model model){
        model.addAttribute("equipe",  equipeRepository.findAll());

        model.addAttribute("spectateur", new Spectateur());
        return "formSpectateur";
    }
    @RequestMapping(value="/savespectateur",method=RequestMethod.POST)
    public String save(Model model ,@Valid @ModelAttribute("spectateur") Spectateur spectateur,
                       BindingResult bindingResult ){
        if(bindingResult.hasErrors())
            return "spectateur";


        // définir le pays de la e

        spectateurRepository.save(spectateur);

        return "redirect:/indexSpectateur";
    }




}
