package isi.tn.qatar2020project.services;

import isi.tn.qatar2020project.entities.Arbitre;
import isi.tn.qatar2020project.entities.Match;

import java.util.List;

public interface Matchservice {

    public Match create(Match match);
    public Match delete(Long id) ;
    public List<Match> findAll();
    public Match update(Match match) ;
    public Match findById(Long id);
}
