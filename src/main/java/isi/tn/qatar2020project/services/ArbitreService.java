package isi.tn.qatar2020project.services;

import isi.tn.qatar2020project.entities.Arbitre;

import java.util.List;

public interface ArbitreService {

        public Arbitre create(Arbitre arbitre);
        public Arbitre delete(Long id) ;
        public List<Arbitre> findAll();
        public Arbitre update(Arbitre arbitre) ;
        public Arbitre findById(Long id);
    }


