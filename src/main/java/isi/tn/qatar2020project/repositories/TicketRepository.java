package isi.tn.qatar2020project.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TicketRepository extends JpaRepository<isi.tn.qatar2020project.entities.Ticket, Long> {
    @Query("select p from Ticket p where p.idticket = :x")
    public Page<isi.tn.qatar2020project.entities.Ticket> chercher(@Param("x") String mc, Pageable pageable);
    @Query("select p from Ticket p where p.idticket = :x")
    public isi.tn.qatar2020project.entities.Ticket searchById(@Param("x") Long id);
}
