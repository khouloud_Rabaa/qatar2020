package isi.tn.qatar2020project.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JoueMatchRepository extends JpaRepository<isi.tn.qatar2020project.entities.JoueMatch, Long> {

    @Query("select p from JoueMatch p where p.idjouematch = :x")
    public Page<isi.tn.qatar2020project.entities.JoueMatch> chercher(@Param("x") String mc, Pageable pageable);
    @Query("select p from JoueMatch p where p.idjouematch = :x")
    public isi.tn.qatar2020project.entities.JoueMatch searchById(@Param("x") int id);
}
