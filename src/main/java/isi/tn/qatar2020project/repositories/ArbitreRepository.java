package isi.tn.qatar2020project.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import isi.tn.qatar2020project.entities.Arbitre;


public interface ArbitreRepository extends JpaRepository<Arbitre, Long> {


    @Query("select p from Arbitre p where p.nomarbitre like :x")
    public Page<isi.tn.qatar2020project.entities.Arbitre> chercher(@Param("x") String mc, Pageable pageable);
    @Query("select p from Arbitre p where p.nomarbitre like :x")
    public isi.tn.qatar2020project.entities.Arbitre searchById(@Param("x") int id);

    void delete(int id);
}
