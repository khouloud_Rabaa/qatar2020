package isi.tn.qatar2020project.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EquipeRepository extends JpaRepository<isi.tn.qatar2020project.entities.Equipe, Long> {
    @Query("select p from Equipe p where p.nomequipe like :x")
    public Page<isi.tn.qatar2020project.entities.Equipe> chercher(@Param("x") String mc, Pageable pageable);
    @Query("select p from Equipe p where p.nomequipe like :x")
    public isi.tn.qatar2020project.entities.Equipe searchById(@Param("x") Long id);

    void delete(int id);
}
