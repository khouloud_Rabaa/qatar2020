package isi.tn.qatar2020project.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SpectateurRepository extends JpaRepository<isi.tn.qatar2020project.entities.Spectateur, Long> {
    @Query("select p from Spectateur p where p.nomspcet like :x")
    public Page<isi.tn.qatar2020project.entities.Spectateur> chercher(@Param("x") String mc, Pageable pageable);
    @Query("select p from Spectateur p where p.nomspcet like :x")
    public isi.tn.qatar2020project.entities.Spectateur searchById(@Param("x") int  id);

    void delete(Long id);
}
