package isi.tn.qatar2020project.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MatchRepository extends JpaRepository<isi.tn.qatar2020project.entities.Match, Long> {

    @Query("select p from Match p where p.lieumatch like :x")
    public Page<isi.tn.qatar2020project.entities.Match> chercher(@Param("x") String mc, Pageable pageable);
    @Query("select p from Match p where p.lieumatch like :x")
    public isi.tn.qatar2020project.entities.Match searchById(@Param("x") int id);
}
